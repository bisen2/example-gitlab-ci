// calculator library to be mocked

#ifndef __CALCULATOR_HPP__
#define __CALCULATOR_HPP__

class Calculator {
public:
  virtual float add(float input1, float input2) { return input1 + input2; }
  virtual float subtract(float input1, float input2) { return input1 - input2; }
  virtual float multiply(float input1, float input2) { return input1 * input2; }
  virtual float divide(float input1, float input2) { return input1 / input2; }
};

#endif // __CALCULATOR_HPP__