// class to be tested

#ifndef __SQUARE_HPP__
#define __SQUARE_HPP__

#include "calculator.hpp"

class Square {
public:
  float side_length;

  Square(float side_length) { this->side_length = side_length; }

  float get_area(Calculator *calc) {
    return calc->multiply(this->side_length, this->side_length);
  }
};

#endif // __SQUARE_HPP__