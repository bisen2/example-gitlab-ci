// test of Square class

#include <cmath>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "calculator.hpp"
#include "square.hpp"

class MockCalc : public Calculator {
public:
  MockCalc() {}
  MOCK_METHOD(float, add, (float, float), (override));
  MOCK_METHOD(float, subtract, (float, float), (override));
  MOCK_METHOD(float, multiply, (float, float), (override));
  MOCK_METHOD(float, divide, (float, float), (override));
};

using ::testing::_;
using ::testing::Return;
using ::testing::StrictMock;

TEST(SquareTests, floatConstr) {
  float side_length = 1.0;
  const Square sq(side_length);
  EXPECT_FLOAT_EQ(side_length, sq.side_length);
}

TEST(SquareTests, getAreaCallsMultiply) {
  StrictMock<MockCalc> calc;
  float side_length = 1.0;

  EXPECT_CALL(calc, multiply(side_length, side_length))
      .WillOnce(Return(side_length * side_length));
  Square sq(side_length);
  EXPECT_FLOAT_EQ(side_length * side_length, sq.get_area(&calc));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
