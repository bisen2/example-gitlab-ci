"""This module tests the classes from the shapes module"""
from math import sqrt
import pytest  # type: ignore
from shapes import RegularPolygon, Square


class TestRegularPolygon:
    """Contains tests for the shapes.RegularPolygon class"""

    @pytest.mark.parametrize("num_sides, side_length", [(1, 1)])
    def test_constructor(self, num_sides, side_length): # pylint: disable=no-self-use
        """Test that shapes.RegularPolygon's constructor sets the class's members"""
        reg_polygon = RegularPolygon(num_sides, side_length)
        assert reg_polygon.num_sides == num_sides
        assert reg_polygon.side_length == side_length

    @pytest.mark.parametrize("num_sides, side_length, result", [(4, 1, 1), (4, 2, 4)])
    def test_get_area(self, num_sides, side_length, result): # pylint: disable=no-self-use
        """Test that shapes.RegularPolygon.get_area returns the correct value"""
        reg_polygon = RegularPolygon(num_sides, side_length)
        assert abs(reg_polygon.get_area() - result) < 0.00001

    @pytest.mark.parametrize("num_sides, result", [(3, 60), (4, 90)])
    def test_get_internal_angle(self, num_sides, result): # pylint: disable=no-self-use
        """Test that shapes.RegularPolygon.get_internal_angle returns the correct value"""
        reg_polygon = RegularPolygon(num_sides, 1)
        assert abs(reg_polygon.get_internal_angle() - result) < 0.00001


class TestSquare:
    """Contains tests for the shapes.Square class"""

    @pytest.mark.parametrize("side_length", [1, 2, 3, 4, 5])
    def test_constructor(self, side_length): # pylint: disable=no-self-use
        """Test that shapes.Square's constructor sets the member side_length"""
        inst_square = Square(side_length)
        assert inst_square.num_sides == 4
        assert inst_square.side_length == side_length

    @pytest.mark.parametrize("side_length, result", [(1, sqrt(2)), (2, sqrt(8))])
    def test_get_diagonal(self, side_length, result): # pylint: disable=no-self-use
        """Test that shapes.Square.get_diagonal returns the correct value"""
        inst_square = Square(side_length)
        assert abs(inst_square.get_diagonal() - result) < 0.00001
