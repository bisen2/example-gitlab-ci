"""This module contains the sample classes that will be tested in Gitlab CI"""

import math

class RegularPolygon:
    """This class acts as a base class for all regular polygons"""

    def __init__(self, num_sides, side_length):
        self.num_sides = num_sides
        self.side_length = side_length

    def get_area(self):
        """Calculate the area of the polygon using self.side_length and self.num_sides"""
        return 0.25 * self.num_sides * (self.side_length)**2 / math.tan(math.pi / self.num_sides)

    def get_internal_angle(self):
        """Calculate the internal angle of the polygon using self.num_sides"""
        return (self.num_sides - 2) * 180 / self.num_sides

class Square(RegularPolygon):
    """This class describes a square"""

    def __init__(self, side_length):
        super().__init__(4, side_length)
        self.num_sides = 4
        self.side_length = side_length

    def get_diagonal(self):
        """Calculate the diagonal of the square"""
        return self.side_length * math.sqrt(2)
